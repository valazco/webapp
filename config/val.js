/**
 * VAL configuration
 * (sails.config.val)
 */

module.exports.val = {

  initialAmount: 180,

  dailyAmount: 6,

  maxInactivity: 30*24*60*60*1000, // 30 days

  reactivateTime: 30*24*60*60*1000, // 30 days

  decreasePercentage: 6,

  decreaseTime: 90*24*60*60*1000, // 90 days,
  
};
