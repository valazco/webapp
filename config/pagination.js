/**
 * Pagination configuration
 * (sails.config.pagination)
 */

module.exports.pagination = {

  paginationSize: 50,

  adminPaginationSize: 150,

  userDefaultPaginationSize: 20
  
};
