/**
 * Roles configuration
 * (sails.config.roles)
 */

module.exports.roles = {

  default: {
    login: true,
    admin: false,
    changeRoles: false,
    deleteUsers: false,
    
    notifications: {
        mailingList: false,
    },
  },
  
  valazco: {
    login: false,
    admin: false,
    changeRoles: false,
    deleteUsers: false,
    
    notifications: {},
  },
    
  founder: {
    login: true,
    admin: true,
    changeRoles: true,
    deleteUsers: true,
    
    notifications: {
      mailingList: true,
      newUser: true,
    }
  },
  
  admin: {
    login: true,
    admin: true,
    changeRoles: false,
    deleteUsers: true,
    
    notifications: {
        mailingList: false,
        newUser: true,
    },
  },
  
};
