parasails.registerPage('signup', {
  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: {
    // Main syncing/loading state for this page.
    syncing: false,

    // Form data
    formData: {
      fullName: '',
      email: '',
      username: '',
      password: '',
      confirmPassword: '',
      isPrivacyPolicyAccepted: null,
      isMailingListAccepted: false
    },

    // For tracking client-side validation errors in our form.
    // > Has property set to `true` for each invalid property in `formData`.
    formErrors: { /* ... */ },

    // Form rules
    formRules: {
      fullName: {required: true},
      email: {required: true, isEmail: true},
      username: {required: true},
      password: {required: true},
      confirmPassword: {required: true, sameAs: 'password'},
      isPrivacyPolicyAccepted: {required: true},
      isMailingListAccepted: {required: false},
    },

    // Server error state
    cloudError: '',

    // Success state when form has been submitted
    cloudSuccess: false,
  },

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    //…
  },
  mounted: async function() {
    //…
  },

  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    isEmailAlreadyRegistered: async function() {
      if(this.formData.email !== ''){
        var found = await Cloud.isAlreadyRegistered.with({email: this.formData.email});

        this.formErrors.emailAlreadyRegistered = found;
      }
      else {
        this.formErrors.emailAlreadyRegistered = false;
      }
    },

    isUsernameAlreadyRegistered: async function() {
      if(this.formData.username !== ''){
        var found = await Cloud.isAlreadyRegistered.with({username: this.formData.username});

        this.formErrors.usernameAlreadyRegistered = found;
      }
      else {
        this.formErrors.usernameAlreadyRegistered = false;
      }
    },
    
    isUsernameValid: async function() {
      if(this.formData.username !== ''){
        let re = /^[a-z]+[a-z0-9_]*[a-z0-9]$/;
        if (re.test(this.formData.username)) {
          this.formErrors.usernameNotValid = false;
        }
        else {
          this.formErrors.usernameNotValid = true;
        }
      }
      else {
        this.formErrors.usernameNotValid = false;
      }
    },

    submittedForm: async function() {
      if(this.isEmailVerificationRequired) {
        // If email confirmation is enabled, show the success message.
        this.cloudSuccess = true;
      }
      else {
        // Otherwise, redirect to the logged-in dashboard.
        // > (Note that we re-enable the syncing state here.  This is on purpose--
        // > to make sure the spinner stays there until the page navigation finishes.)
        this.syncing = true;
        window.location = '/';
      }
    },

  }
});
