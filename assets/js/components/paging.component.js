/**
 * <paging>
 * -----------------------------------------------------------------------------
 * Show paging
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('paging', {

  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'pagination'
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      pages: this.pagination.pages,
      page: this.pagination.page,
      prev: this.pagination.prev,
      next: this.pagination.next
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
  <nav aria-label="Page navigation" class="nav justify-content-center mt-3 overflow-auto">
    <ul class="pagination pagination-sm">
      <!-- PREV BUTTON -->
      <li v-if="prev" class="page-item" :class="prev === page ? 'disabled' : ''">
        <a class="page-link" :href="'?page='+prev" aria-label="<%= __('previous') %>">
          <span aria-hidden="true">&laquo;</span>
          <span class="sr-only"><%= __('previous') %></span>
        </a>
      </li>
      <!-- // PREV BUTTON -->
      <li class="page-item" :class="n == page ? 'disabled' : ''" v-for="n in pages"><a class="page-link" :href="'?page='+n">{{ n }}</a></li>
      <!-- NEXT BUTTON -->
      <li v-if="next" class="page-item" :class="next === page ? 'disabled' : ''">
        <a class="page-link" :href="'?page='+next" aria-label="<%= __('next') %>">
          <span aria-hidden="true">&raquo;</span>
          <span class="sr-only"><%= __('next') %></span>
        </a>
      </li>
      <!-- // NEXT BUTTON -->
    </ul>
  </nav>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    if (this.pages === undefined || this.page === undefined) {
      throw new Error('Incomplete usage of <paging>.`');
    }
  },

  beforeDestroy: function() {

  },

  watch: {

  },


  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

  }

});

