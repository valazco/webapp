/**
 * <show-date>
 * -----------------------------------------------------------------------------
 * A human-readable, self-updating timestamp
 *
 * @type {Component}
 * -----------------------------------------------------------------------------
 */

parasails.registerComponent('showDate', {

  //  ╔═╗╦═╗╔═╗╔═╗╔═╗
  //  ╠═╝╠╦╝║ ║╠═╝╚═╗
  //  ╩  ╩╚═╚═╝╩  ╚═╝
  props: [
    'at',// « The JS timestamp to format
    'show-time',
    'locale'
  ],

  //  ╦╔╗╔╦╔╦╗╦╔═╗╦    ╔═╗╔╦╗╔═╗╔╦╗╔═╗
  //  ║║║║║ ║ ║╠═╣║    ╚═╗ ║ ╠═╣ ║ ║╣
  //  ╩╝╚╝╩ ╩ ╩╩ ╩╩═╝  ╚═╝ ╩ ╩ ╩ ╩ ╚═╝
  data: function (){
    return {
      _localeString: '',
      _showTime: false
    };
  },

  //  ╦ ╦╔╦╗╔╦╗╦
  //  ╠═╣ ║ ║║║║
  //  ╩ ╩ ╩ ╩ ╩╩═╝
  template: `
  <span>{{formattedTimestamp}}</span>
  `,

  //  ╦  ╦╔═╗╔═╗╔═╗╦ ╦╔═╗╦  ╔═╗
  //  ║  ║╠╣ ║╣ ║  ╚╦╝║  ║  ║╣
  //  ╩═╝╩╚  ╚═╝╚═╝ ╩ ╚═╝╩═╝╚═╝
  beforeMount: function() {
    if (this.at === undefined) {
      throw new Error('Incomplete usage of <show-date>:  Please specify `at` as a JS timestamp (i.e. epoch ms, a number).  For example: `<show-date :at="something.createdAt">`');
    }

    if(this.locale === undefined) {
      this._localeString = 'it';
    }
    else {
      // TODO: catch session locale
      this._localeString = this.locale;
    }

    this._showTime = this.showTime;

    this._formatDate();
  },

  beforeDestroy: function() {

  },

  watch: {
    at: function() {
      // Render to account for after-mount programmatic changes to `at`.
      this._formatDate();
    }
  },


  //  ╦╔╗╔╔╦╗╔═╗╦═╗╔═╗╔═╗╔╦╗╦╔═╗╔╗╔╔═╗
  //  ║║║║ ║ ║╣ ╠╦╝╠═╣║   ║ ║║ ║║║║╚═╗
  //  ╩╝╚╝ ╩ ╚═╝╩╚═╩ ╩╚═╝ ╩ ╩╚═╝╝╚╝╚═╝
  methods: {

    _formatDate: function() {
      var options = { dateStyle: 'short' };

      if (this._showTime) {
        options.timeStyle = 'medium';
      }
      
      this.formattedTimestamp = this.at > 0 ? Intl.DateTimeFormat(this._localeString, options).format(this.at) : '-';
    }

  }

});
