module.exports = {


  friendlyName: 'View account overview',


  description: 'Display "Account Overview" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/account/account-overview',
    }

  },


  fn: async function () {

    let activateAtForecast = await sails.helpers.user.calculateActivateAt(Date.now());

    return {
      activateAtForecast: activateAtForecast,
    };

  }


};
