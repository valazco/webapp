module.exports = {


  friendlyName: 'View edit notifications',


  description: 'Display "Edit notifications" page.',


  exits: {

    success: {
      viewTemplatePath: 'pages/account/edit-notifications',
    }

  },


  fn: async function () {

    return {};

  }


};
