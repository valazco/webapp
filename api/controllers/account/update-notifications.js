module.exports = {


  friendlyName: 'Update profile',


  description: 'Update the profile for the logged-in user.',


  inputs: {
  
    newSettings: {
      type: 'ref'
    },

  },


  exits: {

    

  },


  fn: async function ({newSettings}) {
    const notifications = this.req.me.notifications;
    
    Object.entries(newSettings).forEach(([key, value]) => {
      notifications[key] = value;
    });

    // Save to the db
    await User.updateOne({id: this.req.me.id })
    .set({
      notifications
    });

  }


};
