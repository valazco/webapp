module.exports = {


  friendlyName: 'Check if user already exists',


  description: 'Search user by email or username',


  inputs: {

    email: {
      type: 'string',
      description: 'email address',
      required: false
    },

    username: {
      type: 'string',
      description: 'username',
      required: false
    }

  },

  exits: {

    success: {
      description: 'User (not) found'
    },

  },


  fn: async function (inputs, exits) {
    let found = false;

    if (inputs.username && await sails.helpers.user.isReserved(inputs.username)) {
      found = true;
    }
    else {
      let userSearchCriteria = inputs.email ? {'email': inputs.email} : {'username': inputs.username};
      let user = await User.findOne(userSearchCriteria);

      if (user && user.id) {
        found = true;
      }
    }

    return exits.success(found);
  }


};

