module.exports = {


  friendlyName: 'Update profile',


  description: 'Update the profile for the logged-in user.',


  inputs: {

    fullName: {
      type: 'string'
    },

    email: {
      type: 'string'
    },

    username: {
      type: 'string'
    },

  },


  exits: {

    emailAlreadyRegistered: {
      statusCode: 409,
      description: 'The email already exists.',
    },

    usernameAlreadyRegistered: {
      statusCode: 409,
      description: 'The username already exists.',
    },

  },


  fn: async function ({fullName, email, username}) {

    let newEmailAddress = email;
    if (newEmailAddress !== undefined) {
      newEmailAddress = newEmailAddress.toLowerCase();
    }

    let newUsername = username;
    if (newUsername !== undefined) {
      newUsername = newUsername.toLowerCase();
    }

    // Determine if this request wants to change the current user's email address,
    // revert her pending email address change, modify her pending email address
    // change, or if the email address won't be affected at all.
    let desiredEmailEffect;// ('change-immediately', 'begin-change', 'cancel-pending-change', 'modify-pending-change', or '')
    if (
      newEmailAddress === undefined ||
      (this.req.me.emailStatus !== 'change-requested' && newEmailAddress === this.req.me.email) ||
      (this.req.me.emailStatus === 'change-requested' && newEmailAddress === this.req.me.emailChangeCandidate)
    ) {
      desiredEmailEffect = '';
    } else if (this.req.me.emailStatus === 'change-requested' && newEmailAddress === this.req.me.email) {
      desiredEmailEffect = 'cancel-pending-change';
    } else if (this.req.me.emailStatus === 'change-requested' && newEmailAddress !== this.req.me.email) {
      desiredEmailEffect = 'modify-pending-change';
    } else if (!sails.config.custom.verifyEmailAddresses || this.req.me.emailStatus === 'unconfirmed') {
      desiredEmailEffect = 'change-immediately';
    } else {
      desiredEmailEffect = 'begin-change';
    }


    // If the email address is changing, make sure it is not already being used.
    if (_.contains(['begin-change', 'change-immediately', 'modify-pending-change'], desiredEmailEffect)) {
      let conflictingEmail = await User.findOne({
        or: [
          { email: newEmailAddress },
          { emailChangeCandidate: newEmailAddress }
        ]
      });
      if (conflictingEmail) {
        throw 'emailAlreadyRegistered';
      }
    }

    if (this.req.me.username !== username) {
      let isReserved = await sails.helpers.user.isReserved(username);
      let conflictingUsername = await User.findOne({
        username: username
      });

      if (isReserved || conflictingUsername) {
        throw 'usernameAlreadyRegistered';
      }
    }

    username = newUsername;

    // Start building the values to set in the db.
    // (We always set the fullName if provided.)
    let valuesToSet = {
      fullName,
      username
    };

    switch (desiredEmailEffect) {

      // Change now
      case 'change-immediately':
        _.extend(valuesToSet, {
          email: newEmailAddress,
          emailChangeCandidate: '',
          emailProofToken: '',
          emailProofTokenExpiresAt: 0,
          emailStatus: this.req.me.emailStatus === 'unconfirmed' ? 'unconfirmed' : 'confirmed'
        });
        break;

      // Begin new email change, or modify a pending email change
      case 'begin-change':
      case 'modify-pending-change':
        _.extend(valuesToSet, {
          emailChangeCandidate: newEmailAddress,
          emailProofToken: await sails.helpers.strings.random('url-friendly'),
          emailProofTokenExpiresAt: Date.now() + sails.config.custom.emailProofTokenTTL,
          emailStatus: 'change-requested'
        });
        break;

      // Cancel pending email change
      case 'cancel-pending-change':
        _.extend(valuesToSet, {
          emailChangeCandidate: '',
          emailProofToken: '',
          emailProofTokenExpiresAt: 0,
          emailStatus: 'confirmed'
        });
        break;

      // Otherwise, do nothing re: email
    }

    // Save to the db
    await User.updateOne({id: this.req.me.id })
    .set(valuesToSet);

    // If an email address change was requested, and re-confirmation is required,
    // send the "confirm account" email.
    if (desiredEmailEffect === 'begin-change' || desiredEmailEffect === 'modify-pending-change') {
      await sails.helpers.email.sendTemplateEmail.with({
        from: sails.config.custom.internalEmailAddress,
        fromName: sails.config.custom.internalEmailAddress,
        to: newEmailAddress,
        subject: sails.__('mail.changeRequested.subject'),
        template: 'email-verify-new-email',
        templateData: {
          fullName: fullName||this.req.me.fullName,
          token: valuesToSet.emailProofToken
        }
      });
    }

  }


};
