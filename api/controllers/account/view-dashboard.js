module.exports = {


  friendlyName: 'View dashboard',


  description: 'Display dashboard.',

  inputs: {

    page: {
      type: 'number'
    },
    
    attendance: {
      type: 'string'
    }

  },

  exits: {

    success: {
      statusCode: 200,
      description: 'Registered user, so show the dashboard page.',
      viewTemplatePath: 'pages/account/dashboard'
    },

  },


  fn: async function (inputs, exits) {

    let user = await User.findOne(this.req.me.id).populate('attendances');
    let transactionFilter = {or: [{from: user.id}, {to: user.id}]};
    let pagination = await sails.helpers.pagination.with({
      model: Transaction, 
      selectedPage: inputs.page, 
      paginationSize: sails.config.pagination.userDefaultPaginationSize, 
      where: transactionFilter
    });

    let foundTransactions = await Transaction.find()
                                              .where(transactionFilter)
                                              .skip(pagination.start)
                                              .limit(pagination.size)
                                              .sort([{createdAt: 'DESC'}])
                                              .populate('from').populate('to');

    let transactions = [];

    for (let i = 0; i < foundTransactions.length; i++){
      let t = foundTransactions[i];

      let isPayin = await sails.helpers.transaction.isPayin(user, t);
      let withUser = isPayin ? t.from : t.to;
      let createdAt = t.createdAt;

      transactions.push({
        date: createdAt,
        amount: t.amount,
        reason: t.reason,
        isDecrease: !isPayin && withUser && withUser.id === sails.config.user.valazcoUser,
        isPayin: isPayin,
        withId: withUser ? withUser.id : null,
        withUsername: withUser ? withUser.username : null,
        withFullName: withUser ? withUser.fullName : null,
        disabledUser: withUser && withUser.id === sails.config.user.valazcoUser,
      });
    }

    let activateAtForecast = await sails.helpers.user.calculateActivateAt(Date.now());
    
    let attendance = inputs.attendance ? await Attendance.findOne({token: inputs.attendance}) : null;
    let attendances = user.attendances.filter(a => !a.isAttendanceFeeSent);
    
    return exits.success({
      activateAtForecast: activateAtForecast,
      transactions: transactions,
      pagination: pagination,
      attendance: attendance,
      attendances: attendances
    });

  }


};
