module.exports = {


  friendlyName: 'Signup',


  description: 'Sign up for a new user account.',


  extendedDescription:
`This creates a new user record in the database, signs in the requesting user agent
by modifying its [session](https://sailsjs.com/documentation/concepts/sessions), and
(if emailing with Mailgun is enabled) sends an account verification email.

If a verification email is sent, the new user's account is put in an "unconfirmed" state
until they confirm they are using a legitimate email address (by clicking the link in
the account verification message.)`,


  inputs: {

    username: {
      required: true,
      type: 'string',
      description: 'Username.',
    },

    email: {
      required: true,
      type: 'string',
      isEmail: true,
      description: 'The email address for the new account, e.g. m@example.com.',
      extendedDescription: 'Must be a valid email address.',
    },

    password: {
      required: true,
      type: 'string',
      maxLength: 200,
      example: 'passwordlol',
      description: 'The unencrypted password to use for the new account.'
    },

    fullName:  {
      required: true,
      type: 'string',
      example: 'Frida Kahlo de Rivera',
      description: 'The user\'s full name.',
    },
    
    isPrivacyPolicyAccepted: {
      required: true,
      type: 'boolean'
    },
    
    isMailingListAccepted: {
      required: true,
      type: 'boolean'
    }

  },


  exits: {

    success: {
      description: 'New user account was created successfully.'
    },

    invalid: {
      responseType: 'badRequest',
      description: 'The provided fullName, password and/or email address are invalid.',
      extendedDescription: 'If this request was sent from a graphical user interface, the request '+
      'parameters should have been validated/coerced _before_ they were sent.'
    },
    
    privacyNotAccepted: {
      responseType: 'badRequest',
    },

    alreadyRegistered: {
      statusCode: 409,
      description: 'The account already exists.',
    },

  },


  fn: async function ({username, email, password, fullName, isPrivacyPolicyAccepted, isMailingListAccepted}) {

    if (!isPrivacyPolicyAccepted) {
      throw 'privacyNotAccepted';
    }
    
    let newEmailAddress = email.toLowerCase();
    let newUsername = username ? username.toLowerCase() : null;
    let dateNow = Date.now();
    
    const newUserData = {
      username: newUsername,
      fullName,
      email: newEmailAddress,
      password: await sails.helpers.passwords.hashPassword(password),
      isPrivacyPolicyAccepted: isPrivacyPolicyAccepted || false,
      notifications: sails.config.roles.default.notifications,
      funds: sails.config.val.initialAmount,
      deactivateAt: await sails.helpers.user.calculateDeactivateAt(dateNow),
      decreaseAt: await sails.helpers.user.calculateDecreaseAt(dateNow),
      emailProofToken: await sails.helpers.strings.random('url-friendly'),
      emailProofTokenExpiresAt: dateNow + sails.config.custom.emailProofTokenTTL,
      emailStatus: 'unconfirmed'
    };
    
    if (isMailingListAccepted) {
      newUserData.notifications.mailingList = true;
    }

    // Build up data for the new user record and save it to the database.
    // (Also use `fetch` to retrieve the new ID so that we can use it below.)
    let newUserRecord = await User.create(_.extend(newUserData))
    .intercept('E_UNIQUE', 'alreadyRegistered')
    .intercept({name: 'UsageError'}, 'invalid')
    .fetch();

    // Store the user's new id in their session.
    this.req.session.userId = newUserRecord.id;

    // In case there was an existing session (e.g. if we allow users to go to the signup page
    // when they're already logged in), broadcast a message that we can display in other open tabs.
    if (sails.hooks.sockets) {
      await sails.helpers.broadcastSessionChange(this.req);
    }

    await sails.helpers.email.sendVerifyEmail(newUserRecord.id);
    await sails.helpers.email.sendNotifyNewAccountEmail(newUserRecord.id);

  }

};


