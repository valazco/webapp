module.exports = {


  friendlyName: 'View login',


  description: 'Display "Login" page.',

  inputs: {

    redirectUrl: {
      type: 'string'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/entrance/login',
    },

    redirect: {
      description: 'The requesting user is already logged in.',
      responseType: 'redirect'
    }

  },


  fn: async function ({redirectUrl}) {

    if (this.req.me) {
      throw {redirect: '/'};
    }

    let redirectTo = redirectUrl ? redirectUrl : null;

    return {
      redirectUrl: redirectTo
    };

  }


};

