module.exports = {


  friendlyName: 'Add user to attendance',


  description: 'Add user to attendance.',

  inputs: {

    token: {
      type: 'string',
      require: true
    }

  },


  exits: {

    success: {
      description: 'Added user attendance.',
      responseType: 'redirect'
    },
    
    expired: {
      description: 'Expired attendance link.',
      responseType: 'expired'
    },

  },


  fn: async function (inputs, exits) {        
    let userId = this.req.session.userId;
    let attendance = await Attendance.findOne({token: inputs.token});
    let isValid = await sails.helpers.attendance.isValid(attendance.id, userId);
    
    if (!isValid) {
      throw 'expired';
    }
    
    await sails.helpers.attendance.addUser(attendance.id, userId);

    return exits.success('/?attendance=' + attendance.token);
  }


};
