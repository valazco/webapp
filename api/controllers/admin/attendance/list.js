module.exports = {


  friendlyName: 'View attendances',


  description: 'Show attendances.',

  inputs: {

    

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/admin/attendance/list',
      description: 'Display attendances.'
    },

  },


  fn: async function (inputs, exits) {

    let foundAttendances = await Attendance.find().populate('attendees').populate('owner');

    return exits.success({
      attendances: foundAttendances,
      baseUrl: sails.config.custom.baseUrl
    });

  }


};
