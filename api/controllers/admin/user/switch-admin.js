module.exports = {


  friendlyName: 'Switch user to (not) admin',


  description: 'user by id',


  inputs: {

    id: {
      type: 'number',
      required: true
    }

  },

  exits: {

    success: {
      description: 'Successfully user.'
    },

    notFound: {
      description: 'User not found.'
    },
    
    forbidden: {
      description: 'Insufficient permissions'
    }

  },


  fn: async function (inputs, exits) {
    if (!sails.config.roles[this.req.me.role].changeRoles) {
      throw 'forbidden';
    }

    let user = await User.findOne(inputs.id);

    if(!user || (user.role !== 'default' && user.role !== 'admin')) {
      throw 'notFound';
    }
    
    const role = user.role === 'default' ? 'admin' : 'default';
    const notifications = sails.config.roles[role].notifications;

    await User.updateOne(user.id).set({role, notifications});

    return exits.success({
      user,
    });

  }


};
