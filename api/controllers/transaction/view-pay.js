module.exports = {


  friendlyName: 'View new transaction page',


  description: 'Display the new transaction page.',

  inputs: {

    username: {
      type: 'string'
    },

    amount: {
      type: 'number',
      columnType: 'FLOAT'
    },

    reason: {
      type: 'string'
    },

    page: {
      type: 'number'
    }

  },


  exits: {

    success: {
      viewTemplatePath: 'pages/transaction/pay',
      description: 'Display the new transaction page for authenticated users.'
    },

    redirect: {
      responseType: 'redirect',
      description: 'User is the same, so redirect to the account page.'
    }


  },


  fn: async function (inputs, exits) {

    let user = this.req.me;
    let payeeUsername = inputs.username;

    if (payeeUsername && payeeUsername === user.username) {
      throw {redirect:'/account'};
    }

    let payee = payeeUsername ? await User.findOne({username: payeeUsername}) : null;
    let transactions = [];
    let pagination = null;

    if (payee) {
      let transactionFilter = {
        or: [{from: user.id, to: payee.id}, {from: payee.id, to: user.id}]
      };
      
      pagination = await sails.helpers.pagination.with({
        model: Transaction,
        selectedPage: inputs.page,
        paginationSize: sails.config.pagination.userDefaultPaginationSize,
        where: transactionFilter
      });

      let foundTransactions = await Transaction.find()
        .where(transactionFilter)
        .skip(pagination.start)
        .limit(pagination.size)
        .sort([{createdAt: 'DESC'}])
        .populate('from').populate('to');

      for (let i = 0; i < foundTransactions.length; i++){
        let t = foundTransactions[i];

        let isPayin = await sails.helpers.transaction.isPayin(user, t);
        let createdAt = t.createdAt;

        transactions.push({
          date: createdAt,
          amount: t.amount,
          reason: t.reason,
          isPayin: isPayin
        });
      }
    }

    return exits.success({
      name: user.fullName,
      funds: user.funds,
      payeeId: payee ? payee.id : null,
      payeeUserName: payee ? payee.username : null,
      payeeFullName: payee ? payee.fullName : null,
      amount: inputs.amount && inputs.amount > 0 ? inputs.amount : null,
      reason: inputs.reason ? inputs.reason : null,
      transactions: transactions,
      pagination: pagination
    });

  }


};
