module.exports = {


  friendlyName: 'Create',


  description: 'Create attendance.',


  inputs: {
  
    attendanceId: {
      type: 'number'
    },
    
    userId: {
      type: 'number'
    }
    
  },


  fn: async function (inputs) {
    let dateNow = Date.now(); 
    let attendance = await Attendance.findOne(inputs.attendanceId).populate('attendees');
    let user = await User.findOne(inputs.userId);
    
    if (!attendance || attendance.sendAttendanceFeeAt < dateNow || attendance.isAttendanceFeeSent) {
      return false;
    }
    
    if (attendance.attendees.filter((a) => user.id === a.id).length) {
      return false;
    }
    
    return true;
  }


};

