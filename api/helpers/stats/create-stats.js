module.exports = {


  friendlyName: 'Create stats',


  description: 'Create stats.',


  inputs: {

  },


  exits: {

    success: {
      description: 'Stats created',
    },

  },


  fn: async function (inputs, exits) {
    let thisWeek = await sails.helpers.date.weekStart();
    let thisMonth = await sails.helpers.date.monthStart();
    let thisYear = await sails.helpers.date.yearStart();

    sails.transactionsSum = await sails.helpers.stats.transactionsSum();
    sails.transactionsSumThisWeek = await sails.helpers.stats.transactionsSum(thisWeek);
    sails.transactionsSumThisMonth = await sails.helpers.stats.transactionsSum(thisMonth);
    sails.transactionsSumThisYear = await sails.helpers.stats.transactionsSum(thisYear);

    sails.fundsGlobalSum = await sails.helpers.stats.fundsGlobalSum();
    sails.usersCount = await sails.helpers.stats.usersCount();
    sails.adminsCount = await sails.helpers.stats.adminsCount();

    return exits.success();
  }


};
