module.exports = {


  friendlyName: 'Users count',


  description: 'Users count.',


  inputs: {

  },


  exits: {

    success: {
      description: 'Users count',
    },

  },


  fn: async function (inputs, exits) {
    let total = await User.count();

    return exits.success(total);
  }


};
