module.exports = {


  friendlyName: 'Funds total amount',


  description: 'Funds total amount.',


  inputs: {

  },


  exits: {

    success: {
      description: 'Funds total amount',
    },

  },


  fn: async function (inputs, exits) {
    let total = await User.sum('funds').where({'id':{'!=': sails.config.user.valazcoUser}});

    total = Math.round(total * 100) / 100;

    return exits.success(total);
  }


};
