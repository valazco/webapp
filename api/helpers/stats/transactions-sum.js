module.exports = {


  friendlyName: 'Transactions total amount',


  description: 'Transactions total amount.',


  inputs: {

    from: {
      type: 'number'
    },

  },


  exits: {

    success: {
      description: 'Transactions total amount',
    },

  },


  fn: async function (inputs, exits) {
    let fromDate = inputs.from ? inputs.from : 0;

    let total = await Transaction.sum('amount').where({
      'createdAt': {'>=': fromDate},
      'to':{'!=': sails.config.user.valazcoUser}
    });

    total = Math.round(total * 100) / 100;

    return exits.success(total);
  }


};

