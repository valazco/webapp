module.exports = {


  friendlyName: 'Get day start',


  description: 'Get day start.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m(inputs.date).startOf('day').valueOf();

    return exits.success(n);
  }


};
