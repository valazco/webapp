module.exports = {


  friendlyName: 'Get week start',


  description: 'Get week start.',


  inputs: {

    date: {
      type: 'number',
      required: false,
      default: null
    },

  },


  exits: {

    success: {
      description: '',
    },

  },


  fn: async function (inputs, exits) {
    let m = require('moment');
    let n = m(inputs.date).startOf('week').valueOf();

    return exits.success(n);
  }


};
