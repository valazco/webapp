module.exports = {


  friendlyName: 'Is payin',


  description: '',


  inputs: {
    
    user: {
      type: 'ref'
    },
    
    transaction: {
      type: 'ref'
    }
    
  },


  exits: {

    success: {
      description: 'All done.',
    },

  },


  fn: async function (inputs, exits) {
    let res = false;
    
    if (inputs.transaction && inputs.user) {
      res = inputs.transaction.to && (inputs.transaction.to.id === inputs.user.id);
    }
    
    return exits.success(res);
  }


};

