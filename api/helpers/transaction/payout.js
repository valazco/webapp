module.exports = {

  friendlyName: 'Remove amount to funds',

  description: 'Remove amount to funds.',

  inputs: {
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    },
    amount: {
      description: 'The amount to remove',

      type: 'number',

      required: true
    }
  },

  exits: {

    success: {
      description: 'Successfully payout.',
    },

    zeroAmount: {
      description: 'amount = 0',
    },

    negativeAmount: {
      description: 'Negative amount',
    },

    insufficientFunds: {
      description: 'Insufficient funds',
    },

  },

  fn: async function (inputs, exits) {

    if(inputs.amount === 0) { throw 'zeroAmount'; }

    if(inputs.amount < 0) { throw 'negativeAmount'; }

    let user = await User.findOne(inputs.userId);

    if (!user) { throw 'notFound'; }
    
    let amount = Math.trunc(inputs.amount * 100) / 100;
    let currentFunds = user.funds;
    let newFunds = Math.round((currentFunds - amount) * 100) / 100;

    if(amount > currentFunds) { throw 'insufficientFunds'; }

    await User.updateOne(user.id).set({'funds': newFunds});

    return exits.success({});
  }
};
