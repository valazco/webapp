module.exports = {

  friendlyName: 'Return next activateAt',

  description: 'Return next activateAt.',

  inputs: {
    date: {
      type: 'number',

      required: true
    },
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

  },

  fn: async function (inputs, exits) {
    let date = inputs.date + sails.config.val.reactivateTime;

    return exits.success(date);
  }
};
