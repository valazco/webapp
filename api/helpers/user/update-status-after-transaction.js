module.exports = {

  friendlyName: 'Update active status',

  description: 'Update active status.',

  inputs: {
    userId: {
      description: 'The ID of the user to look up.',

      type: 'number',

      required: true
    },

    date: {
      type: 'number',

      required: true
    },
  },

  exits: {

    success: {
      description: 'The requesting socket is now subscribed to socket broadcasts about the logged-in user\'s session.',
    },

  },

  fn: async function (inputs, exits) {
    let user = await User.findOne(inputs.userId);
    let transactionDate = inputs.date;
    let activateDate = await sails.helpers.user.calculateActivateAt(transactionDate);
    let deactivateDate = await sails.helpers.user.calculateDeactivateAt(transactionDate);

    let userActivateDate = !user.active && user.activateAt === 0 ? activateDate : user.activateAt;
    let userDeactivateDate = userActivateDate ? user.deactivateAt : deactivateDate;

    await User.updateOne({ id: user.id }).set({'lastTransactionAt': transactionDate, 'deactivateAt': userDeactivateDate, 'activateAt': userActivateDate });

    return exits.success({});
  }
};
